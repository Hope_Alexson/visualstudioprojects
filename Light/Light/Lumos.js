﻿function Provodka(power) {
    this.id = getId(); 
    this.lampArray = [];
    this.svetilnikArray = [];
    this.switcherArray = [];

    var provodkaContainer = document.createElement('div');
    provodkaContainer.className = 'provodka';
    document.body.appendChild(provodkaContainer);
    var provodkaTitle = document.createElement('h1');
    provodkaTitle.textContent = 'Проводка ' + this.id;
    provodkaContainer.appendChild(provodkaTitle);
    
  


    alert('Создана проводка напряжением ' + power + ' вольт');

    function getLampsCount(lampArray) {
        return lampArray.length;
    }

    function getSvetilnikCount(svetilnikArray) {
        return svetilnikArray.length;
    }

    function getSwitcherCount(switcherArray) {
        return switcherArray.length;
    }

    function getId () {

        return '_' + Math.random().toString(36).substr(2, 9);
    }

    function switchLight(switcher) {
        provodka.lampArray.forEach(function (item) {
            if (item.id == switcher.lightId){
                item.state = switcher.state;

            var lampDiv = document.getElementById(item.id);
            var switcherIcon = document.createElement('div')
            switcherIcon.className = 'switcher';
            lampDiv.appendChild(switcherIcon);
            }

        });
        provodka.svetilnikArray.forEach(function (item) {
            if (item.id == switcher.lightId) {
                item.state = switcher.state;

                var svetilnikDiv = document.getElementById(item.id);
                var switcherIcon = document.createElement('div')
                switcherIcon.className = 'switcher';
                svetilnikDiv.appendChild(switcherIcon);
            }
            if (item.state) {
                item.music = "play";
            }
            else {
                item.music = "stop";
            }
        });

    }

    function addSwitcher(light) {
        var Switcher = {
            id: getId(),
            state: false,
            lightId: light.id
        }
        provodka.switcherArray.push(Switcher);
        switchLight(Switcher);

    }

    this.getAllElem = function () {
        document.write(getLampsCount(this.lampArray) + ' лампочек, &#010; ' + JSON.stringify(provodka.lampArray) + '&#010;');
        document.write(getSvetilnikCount(this.svetilnikArray) + 'светильников, &#010; ' + JSON.stringify(provodka.svetilnikArray) + '&#010;');
        document.write(getSwitcherCount(this.switcherArray) + ' выключателей &#010; ' + JSON.stringify(provodka.switcherArray) + '&#010;');
    }



    this.addLamp = function (count) {
        var i = 0;
        for (i = 0; i < count; i++) {
            var Lamp = {
                power: 220,
                id: getId(),
                state: true,
                provodkaId: null
            }
            if (power == Lamp.power) {
                Lamp.provodkaId = provodka.id;
                provodka.lampArray.push(Lamp);

                var LampIcon = document.createElement('div')
                LampIcon.className = 'lamp';
                LampIcon.id = Lamp.id;
                provodkaContainer.appendChild(LampIcon);
            }
            else {
                alert('Тут света не будет. Напряжение ' + power + ' V не поддерживается');
            }
        }
    }

    this.addSvetilnik = function (count) {
        var i = 0;
        for (i = 0; i < count; i++){
            var Svetilnik = {
                music: 'play',
                power: 220,
                id: getId(),
                state: true,
                provodkaId: null
            }
            if (power == Svetilnik.power) {
                Svetilnik.provodkaId = provodka.id;
                provodka.svetilnikArray.push(Svetilnik);

                var svetilnikIcon = document.createElement('div')
                svetilnikIcon.className = 'svetilnik';
                svetilnikIcon.id = Svetilnik.id;
                provodkaContainer.appendChild(svetilnikIcon);
            }
            else {
                alert('Тут света не будет. Напряжение ' + power + ' V не поддерживается');
            }
        }
    }

    this.linkSwitchersToLights = function (lightsArray) {
        lightsArray.forEach(function (item, i, lightArray) {
            addSwitcher(item);
        })
    }

    this.onSwitchers = function (switchers)
    {
        switchers.forEach(function (item)
        {
            provodka.switcherArray.forEach(function (switcher)
            {
                if (item.id == switcher.id) {
                    switcher.state = true;
                    switchLight(switcher);
                }               
            })
        })
    }

    this.offSwitchers = function (switchers) {
        switchers.forEach(function (item) {
            provodka.switcherArray.forEach(function (switcher) {
                if (item.id == switcher.id) {
                    switcher.state = false;
                    switchLight(switcher);
                }
            })
        })
    }

    this.getLampToSwitcher = function (switcher) {
        provodka.lampArray.forEach(function (item) {
            if (item.id == switcher.lightId) {
                return item;
            }
            else {
                svetilnikArray.forEach(function (item) {
                    if (item.id == switcher.lightId) {
                        return item;
                    }
                    else {
                        alert('Обнаружен одинокий выключатель!' + JSON.stringify(switcher));
                    }    
                })
            }
        })


    }
  

}

var provodka = new Provodka(220)
provodka.addLamp(5);
provodka.addSvetilnik(3);
provodka.linkSwitchersToLights(provodka.lampArray);
provodka.linkSwitchersToLights(provodka.svetilnikArray);
